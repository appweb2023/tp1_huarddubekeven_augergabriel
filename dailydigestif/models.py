from django.contrib.auth.models import AbstractUser
from django.db import models


class User(AbstractUser):
    email = models.EmailField()
    username = models.CharField(max_length=10, unique=True)
    password = models.CharField(max_length=20)
    avatar = models.CharField(max_length=255, default="media/Default_pfp.svg")

    def __str__(self):
        return 'Nom: %s, Courriel: %s' % (self.username, self.email)


class Category(models.Model):
    name = models.CharField(max_length=255)
    value = models.CharField(max_length=255)

    def __str__(self):
        return 'Nom: %s, Valeur: %s' % (self.name, self.value)


class Country(models.Model):
    name = models.CharField(max_length=255)
    code = models.CharField(max_length=10)

    def __str__(self):
        return 'Nom: %s, Code: %s' % (self.name, self.code)


class UserProfile(models.Model):
    user = models.OneToOneField(User, on_delete=models.PROTECT)
    avatar = models.CharField(max_length=255)

    def __str__(self):
        return 'Utilisateur: %s, Avatar: %s' % (self.user.username, self.avatar)


class UserCountries(models.Model):
    country = models.ForeignKey(Country, on_delete=models.PROTECT)
    user = models.ForeignKey(User, on_delete=models.PROTECT)

    def __str__(self):
        return 'Utilisateur: %s, Nom ville: %s, Code ville: %s' % (self.user.username, self.country.name, self.country.code)


class News(models.Model):
    author = models.CharField(max_length=255, null=True, blank=True)
    title = models.CharField(max_length=255)
    description = models.TextField(null=True, blank=True)
    url = models.URLField()
    urlToImage = models.URLField(null=True, blank=True)
    source = models.CharField(max_length=255)
    publishedAt = models.DateTimeField()
    user = models.ForeignKey(User, on_delete=models.PROTECT, null=True, blank=True)
    deleted = models.BooleanField(default=False)
    createdAt = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return 'Titre: %s, Description: %s, URL: %s, Source: %s, Publié le: %s, Utilisateur: %s Effacé?: %s' % (self.title, self.description, self.url, self.source, self.publishedAt, self.user, self.deleted)


class SharedNews(models.Model):
    news = models.ForeignKey(News, on_delete=models.PROTECT)
    user = models.ForeignKey(User, on_delete=models.PROTECT, related_name='shared_news')
    friend = models.ForeignKey(User, on_delete=models.PROTECT, related_name='received_news')
    comment = models.TextField()
    sharedAt = models.DateTimeField()

    def __str__(self):
        return 'Nouvelle: %s, Partageur: %s, Receveur: %s, Commentaire: %s, Date de partage: %s' % (
            self.news.title, self.user.username, self.friend.username, self.comment, self.sharedAt)


class Friendship(models.Model):
    user = models.ForeignKey(User, on_delete=models.PROTECT, related_name='user_friendships')
    friend = models.ForeignKey(User, on_delete=models.PROTECT, related_name='friend_friendships')
    accepted = models.DateTimeField(null=True, blank=True)

    def __str__(self):
        return 'Utilisateur: %s, Ami: %s, Date acceptation: %s' % (self.user.username, self.friend.username, self.accepted)
