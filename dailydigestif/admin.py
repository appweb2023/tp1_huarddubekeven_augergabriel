from django.contrib import admin
from .models import User, Category, Country, UserProfile, UserCountries, News, SharedNews, Friendship


class UserAdmin(admin.ModelAdmin):
    list_display = ['username', 'email']


admin.site.register(User, UserAdmin)


class CategoryAdmin(admin.ModelAdmin):
    list_display = ['name', 'value']


admin.site.register(Category, CategoryAdmin)


class CountryAdmin(admin.ModelAdmin):
    list_display = ['name', 'code']


admin.site.register(Country, CountryAdmin)


class UserProfileAdmin(admin.ModelAdmin):
    list_display = ['user', 'avatar']


admin.site.register(UserProfile, UserProfileAdmin)


class UserCountriesAdmin(admin.ModelAdmin):
    list_display = ['user', 'country']


admin.site.register(UserCountries, UserCountriesAdmin)


class NewsAdmin(admin.ModelAdmin):
    list_display = ['title', 'author', 'source', 'publishedAt', 'user', 'deleted', 'createdAt']


admin.site.register(News, NewsAdmin)


class SharedNewsAdmin(admin.ModelAdmin):
    list_display = ['news', 'user', 'friend', 'comment', 'sharedAt']


admin.site.register(SharedNews, SharedNewsAdmin)


class FriendshipAdmin(admin.ModelAdmin):
    list_display = ['user', 'friend', 'accepted']


admin.site.register(Friendship, FriendshipAdmin)
