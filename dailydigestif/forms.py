from django import forms
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.core.files.storage import FileSystemStorage
from .models import User

error_css_class = 'alert alert-danger'
LANGUAGE_CODE = 'fr-ca'


class RegisterForm(UserCreationForm):
    class Meta:
        model = User
        fields = ("username", "email", "password1", "password2")


class LoginForm(AuthenticationForm):
    class Meta:
        model = User
        fields = ["username", "password"]
        labels = {"username": "Pseudo", "password": "Mot de passe"}


class SearchForm(forms.Form):
    search = forms.CharField(label="Rechercher", max_length=100, widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Rechercher...'}))

