from django.db.models import QuerySet
from django.shortcuts import render, redirect, get_object_or_404, get_list_or_404
from django.contrib.auth import logout, login
from .forms import RegisterForm, LoginForm, SearchForm
from .models import News, SharedNews, User, Category, Friendship
import TP1_HuardDubeKeven_AugerGabriel.settings as settings
import ast, json, urllib.request
from datetime import datetime

api_key = settings.API_KEY


def index(request):
    if request.method == 'POST':
        category = request.POST.get('category')
        news = get_news(category, False)
    else:
        news = get_news("", False)
    return render(request, "dailydigestif/index.html",
                  {"news": news, "category": get_list_or_404(Category.objects.order_by('name'))})


def login_form(request):
    if not request.user.is_authenticated:
        form = LoginForm(data=request.POST)
        if request.method == 'POST':
            if form.is_valid():
                user = form.get_user()
                login(request, user)
                return redirect('/')
        else:
            form = LoginForm()
        return render(request, 'dailydigestif/login.html',
                      {'form': form, "category": get_list_or_404(Category.objects.order_by('name'))})
    else:
        return redirect('login')


def register(request):
    if not request.user.is_authenticated:
        form = RegisterForm(request.POST)
        if request.method == 'POST':
            if form.is_valid():
                form.save()
                return redirect('login')
        else:
            form = RegisterForm()
        return render(request, 'dailydigestif/register.html',
                      {'form': form, "category": get_list_or_404(Category.objects.order_by('name'))})
    else:
        return redirect('login')


def search_friendship(request):
    if request.user.is_authenticated:
        users = None
        user = get_object_or_404(User, username=request.user.username)
        if request.method == 'POST':
            friend_id = request.POST.get('add_friend')
            friend = get_object_or_404(User, id=friend_id)
            Friendship.objects.get_or_create(user_id=user.id, friend_id=friend.id)
        else:
            search = request.GET.get('search')
            if search:
                users = get_users(search)

        return render(request, "dailydigestif/search_friendship.html", {'users': users, "user": user})
    else:
        return redirect('login')


def search_news(request):
    search = request.GET.get('search')
    if search:
        news = get_news(search, True)
    else:
        news = None
    return render(request, "dailydigestif/search_news.html",
                  {"news": news, "category": get_list_or_404(Category.objects.order_by('name'))})


def news(request):
    if request.user.is_authenticated:
        user = get_object_or_404(User, username=request.user.username)
        pinned = News.objects.filter(user_id=user.id, deleted=False).order_by('-createdAt')
        shared_ids = SharedNews.objects.filter(friend_id=user.id).order_by("-sharedAt")
        shared = []
        if shared_ids:
            for shared_id in shared_ids:
                news = News.objects.get(id=shared_id.news_id)
                shared.append(news)
        return render(request, "dailydigestif/news.html", {"pinned": pinned, "shared": shared,
                                                           "category": get_list_or_404(
                                                               Category.objects.order_by('name'))})
    else:
        return redirect('login')


def share(request, id):
    if request.user.is_authenticated:
        news = get_object_or_404(News, id=id)
        user = get_object_or_404(User, username=request.user.username)
        friendships = Friendship.objects.filter(
            user_id=user.id,
            accepted__isnull=False
        ) | Friendship.objects.filter(
            friend_id=user.id,
            accepted__isnull=False
        )

        friends = []
        if friendships:
            for friendship in friendships:
                if friendship.user_id == user.id:
                    friend = User.objects.get(id=friendship.friend_id)
                else:
                    friend = User.objects.get(id=friendship.user_id)
                friends.append(friend)

        if request.method == 'POST':
            friend_id = request.POST.get('friend')
            friend = User.objects.get(id=friend_id)
            comment = request.POST.get('comment')
            SharedNews.objects.create(user_id=user.id, friend_id=friend.id, news=news, comment=comment,
                                      sharedAt=datetime.now())
            return redirect('news')
        return render(request, 'dailydigestif/share.html', {'news': news, 'friends': friends})
    else:
        return redirect('login')


def profile(request):
    user = get_object_or_404(User, username=request.user.username)
    if request.user.is_authenticated:
        friendship_request = Friendship.objects.filter(friend_id=user.id)
        # if request.method == 'POST':
        #     friend = request.POST.get('friendship')
        #     friend_id = User.objects.get(id=friend.id)
        #     Friendship.objects.get(user_id=user.id, friend_id=friend_id).accepted = True
        return render(request, 'dailydigestif/profile.html',
                      {'profile': request.user, 'friendship_requests': friendship_request,
                       'mediaurl': settings.MEDIA_URL,
                       "category": get_list_or_404(
                           Category.objects.order_by('name'))})
    else:
        return redirect('login')


def loging_out(request):
    logout(request)
    return redirect('/')


def pin(request):
    if request.user.is_authenticated and request.method == 'POST':
        user = get_object_or_404(User, username=request.user.username)
        info_value = request.POST.get('info')
        info = ast.literal_eval(info_value)

        existing_news = News.objects.filter(url=info['url'], user_id=user.id, deleted=False).exists()

        if existing_news:
            return redirect('news')
        source = info['source']['name']
        News.objects.create(
            author=info['author'],
            title=info['title'],
            description=info['description'],
            url=info['url'],
            urlToImage=info['urlToImage'],
            source=source,
            publishedAt=info['publishedAt'],
            user_id=user.id,
        )
        return redirect('news')
    else:
        return redirect('login')


def unpin(request):
    if request.user.is_authenticated and request.method == 'POST':
        id_value = request.POST.get('id')
        news = get_object_or_404(News, id=id_value)
        news.deleted = True
        news.save()
        return redirect('news')
    else:
        return redirect('login')


def unpin_share(request):
    if request.user.is_authenticated and request.method == 'POST':
        id_value = request.POST.get('id_shared')
        shared_news = get_object_or_404(SharedNews, news_id=id_value)
        shared_news.delete()
        return redirect('news')
    else:
        return redirect('login')


def get_news(search, is_search):
    if not is_search:
        if search:
            url = urllib.request.urlopen(f"https://newsapi.org/v2/top-headlines?q={search}&apiKey={api_key}")
        else:
            url = urllib.request.urlopen(f"https://newsapi.org/v2/top-headlines?country=ca&apiKey={api_key}")
    else:
        url = urllib.request.urlopen(f"https://newsapi.org/v2/everything?q={search}&apiKey={api_key}")
    news_api = json.load(url)
    news = []
    for item in news_api['articles']:
        source = item['source']['name']
        news_item = {
            'author': str(item['author']),
            'title': str(item['title']),
            'description': str(item['description']),
            'url': str(item['url']),
            'urlToImage': str(item['urlToImage']),
            'source': source,
            'publishedAt': str(item['publishedAt']),
            'info': str(item)
        }
        news.append(news_item)
    return news


def get_users(search):
    users = User.objects.filter(username__contains=search)
    return users
