# Generated by Django 5.0.2 on 2024-02-24 16:20

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('dailydigestif', '0006_remove_user_profile_pic'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='avatar',
            field=models.CharField(default='media/Default_pfp.svg', max_length=255),
        ),
    ]
