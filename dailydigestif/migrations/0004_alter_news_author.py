# Generated by Django 5.0.2 on 2024-02-20 22:29

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('dailydigestif', '0003_alter_news_user'),
    ]

    operations = [
        migrations.AlterField(
            model_name='news',
            name='author',
            field=models.CharField(blank=True, max_length=255, null=True),
        ),
    ]
