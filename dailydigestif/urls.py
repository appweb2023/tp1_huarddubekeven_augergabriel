from django.urls import include, path
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('login', views.login_form, name='login'),
    path('register', views.register, name='register'),
    path('logout', views.loging_out, name='loging_out'),
    path('news/search', views.search_news, name='search_news'),
    path('profile', views.profile, name='profile'),
    path('news', views.news, name='news'),
    path('news/<int:id>/share', views.share, name='share'),
    path('pin', views.pin, name='pin'),
    path('unpin', views.unpin, name='unpin'),
    path('unpin_share', views.unpin_share, name='unpin_share'),
    path('friendship/search', views.search_friendship, name='search_friends')
]
